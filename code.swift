let name = "Евгений"
let last_name = "Саухин"
let second_name = "Юрьевич"
let sex = "m"
let birth_date = "07.01.2003"
let weight = 52.5
let post = "студент"
let course = 4

var result = "\(last_name.uppercased()) \(name) \(second_name), "

if sex == "m" {
    result += "родился \(birth_date), "
} else {
    result += "родилась \(birth_date), "
}

result += "\(post), обучается на \(course) курсе. Для проверки числа с плавающей точкой: вес \(weight)."

print(result)